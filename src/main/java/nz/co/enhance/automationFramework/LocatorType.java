package nz.co.enhance.automationFramework;

//Used only for WinElement
public enum LocatorType {
    ID,
    ACCESSIBILITYID,
    NAME,
    CLASSNAME, CSSSELECTOR, UIAUTOMATION, TAGNAME, XPATH
}
