package nz.co.enhance.automationFramework.HelperClasses;

import nz.co.enhance.automationFramework.Element;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ReflectionsHelper {

    // Reflections stuff allows nifty ways to access classes, methods and fields.
    // Commonly used only for testing mobile on multiple platforms from the same basic code.


    public static Set<Class<?>> getClassList(String nameSpace) {

        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(nameSpace))));

        return reflections.getSubTypesOf(Object.class);
    }


    //Returns a new instance of a class by telling it which one you want. Very useful for duel-platform mobile
    public static String getQualifiedClassName(Set<Class<?>> classes, String className) {

        for (Object foundClass : classes.toArray()) {
            if (foundClass.toString().contains(className)) {
                return ((Class) foundClass).getName();
            }
        }
        return "Class not found.";
    }

    //The below are used in situations where you know the name of a field you want to retrieve and you specify
    //it in the step def.

    public static String getFieldFromClassAsString(String fieldName, Class clazz) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            return field.get(clazz).toString();
        } catch (Exception e) {
            System.out.println("Field could not be found in reflection - check field name.");
            return null;
        }
    }

    public static String getFieldFromObjectAsString(Object obj, String fieldName) {
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return (String) field.get(obj);
        } catch (Exception e) {
            return null;
        }
    }

    public static void invokeMethodInClass(String methodName, Class<?> clazz) {
        try {
            Method method = clazz.getMethod(methodName, (Class<?>[]) null);
            method.invoke(clazz.newInstance(), (Object[]) null);
        } catch (Exception e) {
            System.out.println("Method could not be found in reflection - check method name.");
        }
    }

    public static Element getElementFromClassByName(Object classToFindIn, String elementName) {
        try {
            Field field = classToFindIn.getClass().getDeclaredField(elementName);
            field.setAccessible(true);
            return (Element) field.get(classToFindIn);
        } catch (Exception e) {
            System.out.println("Could not find element " + elementName + " in class " + classToFindIn.getClass());
        }
        return null;
    }


    public static String[] getFieldFromClassAsStringArray(String fieldName, Class clazz) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            return (String[]) field.get(clazz);
        } catch (Exception e) {
            System.out.println("Field could not be found in reflection - check field name.");
            return null;
        }
    }
}
