package nz.co.enhance.automationFramework.HelperClasses;

import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FileHandler {


    // Use this class to read and write files. The default is UTF-8 however you can specify the format if you need
    // something different.

    public static void writeOutFileAsHTML(Document resource, String fileName) throws IOException {
        File file = new File(fileName);
        FileUtils.writeStringToFile(file, resource.outerHtml(), "UTF-8");
    }

    public static void writeOutFile(String resource, String fileName) throws IOException {
        File file = new File(fileName);
        FileUtils.writeStringToFile(file, resource, "UTF-8");
    }


    public static void writeOutFile(String resource, String fileName, String format) throws IOException {
        File file = new File(fileName);
        FileUtils.writeStringToFile(file, resource, format);
    }

    public static String readFileAsString(String fileName) {
        File file = new File(fileName);
        try {
            return FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String readFileAsString(String fileName, String format) {
        File file = new File(fileName);
        try {
            return FileUtils.readFileToString(file, format);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static InputStream readFileAsInputStream(String fileName) {
        File file = new File(fileName);
        try {
            return FileUtils.openInputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
