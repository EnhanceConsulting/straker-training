package nz.co.enhance.automationFramework.HelperClasses;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.Test;

public class EncryptionHelper {
    // Use this helper to encrypt and decrypt sensitive data such as passwords when they cannot be stored in a test project
    // as cleartext. Don't check in the key! It's best to define the key locally or as a system environment variable in
    // a CI/CD situation.


    // By default Jasypt uses PBEWithMD5AndDES encryption (56 bit).
    public static String decryptPassword(String encryptedPassword, String key) {

        String passwordToDecrypt;

        // Check encrypted password contains 'enc' wrapper.
        if (encryptedPassword.contains("enc(")) {
            // If so remove it so we can decrypt the encrypted value.
            passwordToDecrypt = encryptedPassword.replace("enc(", "").replace(")", "");
        } else {
            // Return the unencrypted string.
            return encryptedPassword;
        }

        // Do we have a valid key?
        if (key != null) {
            // Yes - so use it to decrypt and return the value.
            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword(key);
            return encryptor.decrypt(passwordToDecrypt);
        } else {
            // No - so return the encrypted string.
            return passwordToDecrypt;
        }
    }

    // If you want to quickly encrypt or decrypt you can replace these values and run these locally.
    // Remember not to check in any changes you make.
    @Test
    public void encryptMyString(){
        String stringToEncrypt = "yourClearTextStringHere";
        String key = "YourKeyHere";

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(key);
        System.out.println(encryptor.encrypt(stringToEncrypt));
    }

    @Test
    public void decryptMyString(){
        String stringToDecrypt = "yourEncryptedStringHere";
        String key = "YourKeyHere";

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(key);
        System.out.println(encryptor.decrypt(stringToDecrypt));
    }
}