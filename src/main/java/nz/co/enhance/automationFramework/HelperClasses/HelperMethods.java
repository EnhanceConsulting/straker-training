package nz.co.enhance.automationFramework.HelperClasses;


import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HelperMethods {

    // This class is for general helpful stuff for auto testing that we may reuse frequently.


    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static String createRandomString(int lengthOfString) {
        String randomStringComponents = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(lengthOfString);
        for (int i = 0; i < lengthOfString; i++)
            sb.append(randomStringComponents.charAt(rnd.nextInt(randomStringComponents.length())));
        return sb.toString();
    }

    // Note that this returns the number as a string. If it starts with zero(s) and you parse it as a number then
    // it may end up smaller than you expect, e.g. 00123 will end up as 123 which will be a problem if you need 5 digits
    public static String createRandomNumber(int lengthOfString) {
        String randomNumberComponents = "0123456789";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(lengthOfString);
        for (int i = 0; i < lengthOfString; i++)
            sb.append(randomNumberComponents.charAt(rnd.nextInt(randomNumberComponents.length())));
        return sb.toString();
    }

    // Unlike the above, this will return an integer between (and including) the min and max values.
    // For instance if you wanted a random 5 digit number you would use createRandomInteger(10000, 99999)
    public static int createRandomInteger(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static String createRandomSpecialChars(int lengthOfString) {
        String randomCharsComponents = "!@#$%^&*";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(lengthOfString);
        for (int i = 0; i < lengthOfString; i++)
            sb.append(randomCharsComponents.charAt(rnd.nextInt(randomCharsComponents.length())));
        return sb.toString();
    }

    public static String makeFirstLetterLowercase(String inputString) {
        char c[] = inputString.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

    public static String makeFirstLetterUpperCase(String inputString) {
        char c[] = inputString.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }

    // Converts a String[] into a List<String>. Useful when splitting strings.
    public static List<String> turnStringArrayIntoList(String[] stringArray) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < stringArray.length; i++) {
            list.add(stringArray[i]);
        }
        return list;
    }



    // Nifty and helpful if you need to quickly get a single/first match out of a string
    // E.g. if you need to pull a date out of a file with the format YYYY_mm_DD
    // you could use findRegEx(fileText, "\\d\\d\\d\\d_\\d\\d_\\d\\d", 0)
    public static String findRegEx(String value, String regex, int whichValue) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            return (matcher.group(whichValue));
        }
        return "";
    }

    // An easy way to get the full path to the root directory of where you are running
    // which is sometimes required for file handling. Also returns the correct format
    // for both Windows and Mac so you can use it to make code platform independent.
    public static String getLocalPath() {
        File f = new File("");
        return f.getAbsolutePath();
    }

    // Converts objects into JSON strings. Used for API testing.
    public static String toJSON(Object object) {
        return TextPrettifier.indentJson(new JSONParser(APIHelper.objectToJson(object)).toJsonString());
    }



}


