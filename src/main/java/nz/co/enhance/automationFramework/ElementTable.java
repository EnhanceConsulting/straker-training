package nz.co.enhance.automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ElementTable {
    public List<List<TableElement>> rowsInTable = null;
    public By locator = null;
    public String xpath = null;
    public static WebElement webElement = null;

    //when instantiating the tableElement we DON'T fetch it. This is so we can define elements in a page object before the page is loaded.
    public ElementTable(String xpath) { //do it by locator
        this.xpath = xpath;
    }

    public ElementTable(WebElement element) { //do it by an existing element - less desirable
        webElement = element;
    }

    //won't refresh the table if it's already been built, as the text values won't change
    public boolean isValueInTable(String value) {
        if (rowsInTable == null) {
            fetchRows();
        }
        for (List<TableElement> row : rowsInTable) {
            for (TableElement element : row) {
                if (element.elementText.contains(value)) {
                    return true;
                }
            }
        }
        return false;
    }


    //will always refresh to avoid null pointer errors. This can make it quite slow.
    //only use it if you must have the element and can't be satisfied with the text and attributes, eg clicking.
    public Element getCellElement(int row, int column) {
        fetchRows();
        return rowsInTable.get(row).get(column).element;
    }

    public TableElement getCell(int row, int column) {
        fetchRows();
        return rowsInTable.get(row).get(column);
    }

    //faster option to only retrieve a field of each row and when finding the matching row we just retrieve that one
    public String getFieldOfRow(int row, int field) {
        return fetchRow(row).get(field).elementText;
    }

    //fast row finder with a text search on a field
    public int findRowWithFieldText(int field, String text) {
        int size = Automator.driver.findElements(By.xpath(xpath + "//tr")).size();
        //for each row of the table
        for (int i = 0; i < size; i++) {
            //we don't know if header or regular fields so try both
            try {
                if (Automator.driver.findElements(By.xpath(xpath + "//tr[" + (i + 1) + "]//th")).get(field).getText().contains(text)) {
                    return i;
                }
            } catch (Exception e) {

            }
            try {
                if (Automator.driver.findElements(By.xpath(xpath + "//tr[" + (i + 1) + "]//td")).get(field).getText().contains(text)) {
                    return i;
                }
            } catch (Exception e) {

            }
        }
        return -1;
    }


    //fetch a specific row - remember that 0 will be header
    public List<TableElement> fetchRow(int rowIndex) {
        //how many rows should we have in total?
        int size = Automator.driver.findElements(By.xpath(xpath + "//tr")).size();
        List<TableElement> row = new ArrayList<>();
        //if it's a header, get header elements
        int fieldsInRow = Automator.driver.findElements(By.xpath(xpath + "//tr[" + (rowIndex + 1) + "]//th")).size();
        for (int j = 0; j < fieldsInRow; j++) {
            TableElement te = new TableElement();
            te.element = new Element(By.xpath(xpath + "//tr[" + (rowIndex + 1) + "]/th[" + (j + 1) + "]"));
            te.elementText = te.element.getText();
            te.attributes = te.element.getElementAttributes();
            row.add(te);
        }

        //if it's a normal row, get row elements
        fieldsInRow = Automator.driver.findElements(By.xpath(xpath + "//tr[" + (rowIndex + 1) + "]//td")).size();
        for (int j = 0; j < fieldsInRow; j++) {
            TableElement te = new TableElement();
            te.element = new Element(By.xpath(xpath + "//tr[" + (rowIndex + 1) + "]/td[" + (j + 1) + "]"));
            te.elementText = te.element.getText();
            te.attributes = te.element.getElementAttributes();
            row.add(te);
        }

        return row;
    }


    public void fetchRows() {
        rowsInTable = new ArrayList<>();

        int size = Automator.driver.findElements(By.xpath(xpath + "//tr")).size();

        //for each row of the table
        for (int i = 0; i < size; i++) {
            List<TableElement> row = new ArrayList<>();

            //get headers

            int fieldsInRow = Automator.driver.findElements(By.xpath(xpath + "//tr[" + (i + 1) + "]//th")).size();
            for (int j = 0; j < fieldsInRow; j++) {
                TableElement te = new TableElement();
                te.element = new Element(By.xpath(xpath + "//tr[" + (i + 1) + "]/th[" + (j + 1) + "]"));
                te.elementText = te.element.getText();
                te.attributes = te.element.getElementAttributes();
                row.add(te);
            }

            //get regular fields
            fieldsInRow = Automator.driver.findElements(By.xpath(xpath + "//tr[" + (i + 1) + "]//td")).size();
            for (int j = 0; j < fieldsInRow; j++) {
                TableElement te = new TableElement();
                te.element = new Element(By.xpath(xpath + "//tr[" + (i + 1) + "]/td[" + (j + 1) + "]"));
                te.elementText = te.element.getText();
                te.attributes = te.element.getElementAttributes();
                row.add(te);
            }
            rowsInTable.add(row);
        }
    }

    public class TableElement {
        public String elementText;
        public Element element;
        public List<List<String>> attributes;

        public String getAttribute(String key) {
            for (List<String> attribute : attributes) {
                if (attribute.get(0).toLowerCase().contains(key.toLowerCase())) {
                    return attribute.get(1);
                }
            }
            return ""; //attribute doesn't exist
        }
    }
}
