package nz.co.enhance.Objects.PageObjects.OnDemand;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class LoginAndAuthenticationPage {
    // Common elements.
    public Element emailField = new Element(By.id("email"));
    public Element passwordField = new Element(By.id("password"));

    // Login Form elements
    public Element loginButton = new Element(By.cssSelector("[type=\"submit\"]"));

    // Signup Form elements
    public Element signUpMenu = new Element(By.linkText("Sign Up"));
    public Element firstNameField = new Element(By.cssSelector("[name=\"first_name\"]"));
    public Element lastNameField = new Element(By.cssSelector("[name=\"last_name\"]"));
    public Element ageSelectField = new Element(By.cssSelector("[name=\"yearOfBirth\"]"));
    public Element genderSelectField = new Element(By.cssSelector("[name=\"gender\"]"));
    public Element signupSubscriptionCheckbox = new Element(By.cssSelector("input[name=\"edmSubscription\"]"));
    public Element signUpTermsCheckbox = new Element(By.cssSelector(".auth0-lock-sign-up-terms-agreement input"));
    public Element signUpButton = new Element(By.cssSelector("button.auth0-lock-submit"));

    // Favourites Next Button
    public Element noThanksButton = new Element(By.cssSelector("button[data-test-registration-form-favourites-next-button]"));
    public Element dropDownLockList = new Element(By.xpath("//div[@class=\"auth0-lock-list-code\"]"));

    public LoginAndAuthenticationPage() {
        emailField.waitForElementToBeDisplayed(20);
    }

    public Element getDropdownElementByText(String text) {
        return new Element(By.xpath("//li[text()=\"" + text + "\"]"));
    }
}
