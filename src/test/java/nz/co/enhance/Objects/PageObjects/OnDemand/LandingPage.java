package nz.co.enhance.Objects.PageObjects.OnDemand;

import nz.co.enhance.automationFramework.Element;
import org.openqa.selenium.By;

public class LandingPage {


    public Element logo = new Element(By.className("SiteNav-tvnz-logo"));
    public Element letsGoButton = new Element(By.className("scod-button--primary"));

    public LandingPage() {
        logo.waitForElementToBeDisplayed(20);
        if (letsGoButton.exists(3)) {
            letsGoButton.click();
        }
    }

}
