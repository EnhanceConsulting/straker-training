package nz.co.enhance.Objects.PageObjects;

import nz.co.enhance.automationFramework.Element;
import nz.co.enhance.automationFramework.ElementList;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class StrakerInvestorCalendarPage {

    public Element investorSignUpButton = new Element(By.linkText("INVESTOR NEWS SIGN UP"));


    public StrakerInvestorCalendarPage() {
        investorSignUpButton.waitForElementToExist(20);
    }

    public List<String> getDates() {
        List<String> dates = new ArrayList<>();
        ElementList dateElementList = new ElementList(By.cssSelector("div > p:nth-child(2) > strong"));
        for (Element date : dateElementList.getElementList()) {
            dates.add(date.getText());
        }
        return dates;
    }

    public List<String> getDescriptions() {
        List<String> descriptions = new ArrayList<>();
        ElementList descElementList = new ElementList(By.cssSelector("div > p:nth-child(2) > span"));
        for (Element desc : descElementList.getElementList()) {
            descriptions.add(desc.getText());
        }
        return descriptions;
    }


}
