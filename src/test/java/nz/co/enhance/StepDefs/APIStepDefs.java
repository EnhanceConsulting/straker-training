package nz.co.enhance.StepDefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.DataObjects.ExampleEmployeeRecord;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.HelperClasses.HelperMethods;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.ServiceClasses.GETRequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;
import nz.co.enhance.automationFramework.ServiceClasses.POSTRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class APIStepDefs {

    HTTPRequest request;

    @When("^I send a GET to the test api to get an employee list$")
    public void iSendAGETToTheTestApi() {
        String endpoint = TestDriver.properties.getProperty("testAPIEndpoint") + "employees";

        request = new GETRequest(endpoint, null);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I see the response code is (\\d+)$")
    public void iSeeTheResponseCodeIs(int code) {
        assertEquals(code, request.responseCode);
    }

    @And("^I see the response contains the text (.*)$")
    public void iSeeTheResponseContainsTheText(String text) {
        assertTrue(request.response.contains(text));
    }


    @And("^I see the response contains the following data:$")
    public void iSeeTheResponseContainsTheFollowingData(DataTable dataTable) {
        String results = "";
        boolean result = true;

        List<List<String>> data = dataTable.cells(1); //turn the table into a list, ignoring the first row

        JSONParser j = new JSONParser(request.response);
        for (List<String> pairToCheck : data) {
            String field = pairToCheck.get(0);
            String value = pairToCheck.get(1);
            String actualValue = j.findValue(field);
            if (actualValue.equalsIgnoreCase(value)) {
                results += "PASSED: Field: " + field + " had the value " + value + " \n";
            } else {
                results += "FAILED: Field: " + field + " expected the value " + value + " but actually had: " + actualValue + "\n";
                result = false;
            }
        }

        Hooks.scenario.write(results);
        assertTrue(result);
    }


    @When("^I send a POST request with the following new employee data:$")
    public void iSendAPOSTRequestWithTheFollowingData(DataTable dataTable) {
        String endpoint = TestDriver.properties.getProperty("testAPIEndpoint") + "create";

        // Most Post requests have to specify the type of data they are sending
        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Content-Type", "application/json;"));

        // The table from the step def can be used as a map because we always know what pieces will be included for this
        // API body, and the keys will always be unique.
        Map<String, String> data = dataTable.asMap(String.class, String.class);

        ExampleEmployeeRecord exampleEmployeeRecord = new ExampleEmployeeRecord();
        exampleEmployeeRecord.name = data.get("name");
        exampleEmployeeRecord.salary = data.get("salary");
        exampleEmployeeRecord.age = data.get("age");

        request = new POSTRequest(endpoint, HelperMethods.toJSON(exampleEmployeeRecord), headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());


    }

    @And("^I see the response contained (\\d+) instances of field (.*)$")
    public void iSeeTheResponseContainedInstancesOfFieldId(int expectedCount, String fieldPath) {
        JSONParser j = new JSONParser(request.response);
        int actualCount = j.findValues(fieldPath).size();
        assertEquals("The field " + fieldPath + " should have a count of " + expectedCount, expectedCount, actualCount);
    }

    @And("^I see the (.*) object has the following fields:$")
    public void iSeeTheDataObjectHadTheFollowingFields(String objectName, DataTable dataTable) {
        List<List<String>> data = dataTable.cells(1);

        String results = "";
        boolean result = true;

        JSONParser j = new JSONParser(request.response);

        for (List<String> row : data) {
            String fieldName = row.get(0);
            String fieldType = row.get(1);

            String actualType = j.getTypeOfElement(objectName + "." + fieldName);

            if (actualType.equalsIgnoreCase(fieldType)) {
                results += "PASSED: Field: " + fieldName + " had the type " + fieldType + " \n";
            } else {
                results += "FAILED: Field: " + fieldName + " expected the type " + fieldType + " but actually had: " + actualType + "\n";
                result = false;
            }
        }
        Hooks.scenario.write(results);
        assertTrue(result);
    }
}
