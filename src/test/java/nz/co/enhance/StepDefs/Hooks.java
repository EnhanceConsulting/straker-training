package nz.co.enhance.StepDefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.HelperClasses.DateHelper;


public class Hooks {

    public static TestDriver testDriver;
    public static Scenario scenario;


    @Before
    public void before(Scenario scenario)  {
        this.scenario = scenario; // By storing this in Hooks  we can write pictures and text to the selenium logs easily as well as access Scenario info
        //instantiate a test driver
        testDriver = new TestDriver();
    }


    @After
    public void after() {
        //call quit on the TestDriver here and do any required logging.
        if (testDriver.automator.driver != null) {
            scenario.write("Timestamp at finish: " + DateHelper.getDateInFormat("HH:mm:ss.SSS"));
            if (scenario.getStatus().equalsIgnoreCase("failed")) {
                testDriver.automator.takeScreenshot(scenario);
            }
            testDriver.cleanup();
        }
    }
}
