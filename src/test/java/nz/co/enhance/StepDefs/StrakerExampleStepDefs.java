package nz.co.enhance.StepDefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.PageObjects.StrakerInvestorCalendarPage;
import nz.co.enhance.TestDriver;

import java.util.List;

import static org.junit.Assert.*;

public class StrakerExampleStepDefs {

    @When("^I navigate to the Straker Investor Calendar page$")
    public void iNavigateToTheStrakerInvestorCalendarPage() {
        TestDriver.automator.navigateTo(TestDriver.strakerProperties.getProperty("investorCalendarURL"));
    }

    @Then("^I see the following Investor Dates are displayed:$")
    public void iSeeTheFollowingInvestorDatesAreDisplayed(DataTable dataTable) {
        StrakerInvestorCalendarPage strakerInvestorCalendarPage = new StrakerInvestorCalendarPage();

        List<String> dates = strakerInvestorCalendarPage.getDates();
        List<String> desc = strakerInvestorCalendarPage.getDescriptions();

        List<List<String>> valuesToCheck = dataTable.cells(1);

        for (List<String> valuePair : valuesToCheck) {
            String date = valuePair.get(0);
            String description = valuePair.get(1);


            int index = dates.indexOf(date);
            if (index == -1) {
                fail("The text " + date + "was not found in the list.");
            }

            System.out.println("Index was " + index);
            System.out.println("description at that index was " + desc.get(index));
            assertTrue("The date " + date + "should have the description " + description, desc.get(index).equalsIgnoreCase(description));

        }

    }
}
