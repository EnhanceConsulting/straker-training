package nz.co.enhance.StepDefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.PageObjects.GoogleSearchPage;
import nz.co.enhance.TestDriver;
import nz.co.enhance.automationFramework.Element;
import nz.co.enhance.automationFramework.ElementList;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class GoogleStepDefs {

    @Given("^I navigate to the Google URL$")
    public void i_navigate_to_the_URL() {
        TestDriver.automator.navigateTo(TestDriver.googleProperties.getProperty("googleURL"));
    }

    @When("^I search for the term (.*)$")
    public void iSearchForTheTerm(String searchTerm) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchBox.sendKeys(searchTerm);
        googleSearchPage.searchBox.sendKeys(Keys.ENTER);

    }

    @Then("^I see there is a search result with the text (.*)$")
    public void iSeeThereIsASearchResultWithTheText(String text) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded
        assertTrue("We should see the search term " + text, googleSearchPage.searchResults.getText().contains(text));
    }

    @Then("^I see the search results contain the following words:$")
    public void iSeeTheSearchResultsContainTheFollowingWords(DataTable dataTable) {
        // Because we are passing in a simple, 1-dimensional list of strings, we can turn it into a simple list.
        List<String> termsToFind = dataTable.asList(String.class);

        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded

        for (String term : termsToFind) {
            assertTrue("We should see the search term " + term, googleSearchPage.searchResults.getText().contains(term));
        }

    }

    @Then("^I see the following search results contain the terms:$")
    public void iSeeTheFollowingSearchResultsContainTheTerms(DataTable dataTable) {
        // The data table is multi dimensional and the fields might possibly be the same, so we would use a list of lists
        // rather than a map.

        // The firstrow integer lets you specify if you have a header row (1) or no header (0).
        List<List<String>> data = dataTable.cells(1);


        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded

        //find all the titles on the page
        // Titles have no usable classnames/ids but are always in h3 elements
        ElementList titles = new ElementList(By.cssSelector("h3"));

        for (List<String> resultToCheck : data) {
            String articleName = resultToCheck.get(0); //this is the resultTitle column
            String articleSearchTerm = resultToCheck.get(1); //this is the resultContainsTerm column

            //find the individual search result with that title
            // Titles have no usable classnames/ids but are always in h3 elements
            Element titleElement = titles.getElementWithText(articleName);

            if (titleElement != null) {
                Element parentResult = titleElement.getParent().getParent().getParent(); //go up a few levels
                assertTrue("The search result should contain the text " + articleSearchTerm, parentResult.getText().contains(articleSearchTerm));
            } else {
                fail("The search result with the title " + articleName + " was not displayed.");
            }

        }

    }
}
