Feature: Tests for the Straker training session

  Scenario: Check the dates on the Investor Calendar
    When I navigate to the Straker Investor Calendar page
    Then I see the following Investor Dates are displayed:
      | date                     | description                           |
      | 31 March 2020:            | Financial year-end  FY20      BBEEBBEBEB        |
      | Mid to Late May 2020:     | Release of Financial results for FY20 |
      | Late May/Early June 2020: | FY20 results roadshow  (Aus/NZ)       |