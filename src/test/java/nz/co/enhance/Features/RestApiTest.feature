@api
Feature: Examples of creating Rest requests and parsing JSON

  This uses the Enhance library to set up request objects and send and receive data. RestAssured is a similar library
  that is often used by devs so there are examples using this in another feature file.

  All examples are using Rest APIs because SOAP APIs (using XML) are relatively rare nowdays, however the basic actions would
  remain the same using SOAP except for sending and parsing XML instead of JSON.


  # A get scenario is quite simple to test.
  # Get requests only require an endpoint and any headers that are applicable (sometimes there aren't any required headers if the API is public)
  # The endpoint may contain a query parameter or parameters, which you would send through from the step defs
  # The response is parsed to check the testing conditions are fulfilled, which would include such checks as:
  #   - the response matches the schema (fields, structure, data types)
  #   - the data returned matches our expected data for the request sent, e.g. the correct user details were returned, or the correct
  #     number of records
  Scenario: Send a simple GET request and parse the response
    When I send a GET to the test api to get an employee list
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName | expectedResult |
      | status    | success        |
    And I see the response contained 24 instances of field data.id
    And I see the data object has the following fields:
      | fieldName       | fieldType |
      | id              | number    |
      | employee_name   | string    |
      | employee_salary | number    |
      | employee_age    | number    |
      | profile_image   | string    |


  # A POST request is slightly more work to set up.
  # POSTs will require an endpoint (usually they don't have parameters), applicable headers and a body, always JSON for REST.
  # We send the body as a string so it is possible to read in a existing JSON file or create our own hard-coded body, replace
  # a few variables and send it, however this can cause maintenance issues in the long run and isn't great practice. If the
  # schema of your request changes then you'll have a lot of updating to do!
  #
  # Best practice is to model the request body as an object, send through the parameters to the object in the test
  # then convert the object to JSON to send the data.
  # The below example shows how to do this.
  Scenario: Create a POST request using objects
    When I send a POST request with the following new employee data:
      | fieldName | value             |
      | name      | Tester McTestyson |
      | salary    | 1234              |
      | age       | 29                |
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName   | expectedResult    |
      | status      | success           |
      | data.name   | Tester McTestyson |
      | data.salary | 1234              |
      | data.age    | 29                |
    And I see the data object has the following fields:
      | fieldName | fieldType |
      | id        | number    |
      | name      | string    |
      | salary    | string    |
      | age       | string    |

