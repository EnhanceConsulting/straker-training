package nz.co.enhance;

import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.Automator;
import nz.co.enhance.automationFramework.HelperClasses.PropertiesHandler;

import java.util.Properties;

public class TestDriver {


    // Standard setup for most test projects
    public static Automator automator = new Automator();
    public String runTarget;
    public String env;
    public static ExecutionData executionData = new ExecutionData();

    // Properties you may need to read -specific to the test project you are doing, the below are from the example scenarios
    public static Properties properties;
    public static Properties tvnzProperties;
    public static Properties googleProperties;
    public static Properties strakerProperties;

    public TestDriver() {
        setup();
    }

    public void setup() {
        //properties read
        //get the runtime properties - defines which target you will run your tests against.
        properties = new PropertiesHandler().loadProperties("src/main/resources/global.properties");

        //Set the global property of runTarget, i.e. which browser, mobile device or app type we are testing.
        runTarget = setEnvironmentProperty("runTarget");

        //sets the environment we are running in - currently only used for the TVNZ-based example tests.
        env = setEnvironmentProperty("env");

        //used for the TVNZ tests
        tvnzProperties = new PropertiesHandler().loadProperties("src/main/resources/tvnzData-" + env + ".properties");

        //used for the Google tests
        googleProperties = new PropertiesHandler().loadProperties("src/main/resources/google.properties");

        //used for the Google tests
        strakerProperties = new PropertiesHandler().loadProperties("src/main/resources/straker.properties");


        // This is not mandatory but useful.
        // Any tests tagged  @api will ignore the runTarget, so no more accidentally launching a browser when you just
        // want to run an API test.
        if (Hooks.scenario.getSourceTagNames().contains("@api")) {
            runTarget = "api";
        }


        // Start up the required driver for your testing as indicated in the runTarget
        if (runTarget.equalsIgnoreCase("chrome")) {
            automator.startChrome();
        }

        if (runTarget.equalsIgnoreCase("firefox")) {
            automator.startFirefox();
        }
        if (runTarget.equalsIgnoreCase("safari")) {
            automator.startSafari();
        }
        if (runTarget.equalsIgnoreCase("edge")) {
            automator.startEdge();
        }
        if (runTarget.equalsIgnoreCase("ie")) {
            //You must follow these steps before IE will work:
            // https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver#required-configuration

            // The automator class sets some of the required options but you can override if you wish by setting them
            // here and passing them through as InternetExplorerOptions
            automator.startIE();
        }


        // Once all the browsers have been set up and you have a running automator then check the flags
        if (automator.driver != null) {
            //sets if you want web screenshots to be full page or just what is currently displayed. Default is false.
            automator.takeFullPageScreenshots = setBooleanEnvironmentProperty("takeFullPageScreenshots");

        }
    }


    public String setEnvironmentProperty(String propertyName) {
        if (System.getenv().containsKey(propertyName)) {
            return System.getenv(propertyName);
        } else if (System.getenv().containsKey(propertyName.toUpperCase())) {   //some instances of windows are uppercasing runtarget
            return System.getenv(propertyName.toUpperCase());
        } else {
            return properties.getProperty(propertyName);
        }
    }

    public Boolean setBooleanEnvironmentProperty(String propertyName) {
        if (System.getenv().containsKey(propertyName)) {
            return Boolean.valueOf(System.getenv(propertyName));
        } else {
            return Boolean.valueOf(properties.getProperty(propertyName));
        }
    }

    public void cleanup() {
        //if we need any logging or extra reporting we can pop it here.
        if (automator != null) {
            automator.quit();
        }
    }
}
